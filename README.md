# 商品评论采集助手

#### 介绍
商品评论采集助手，可采集京东、天猫、淘宝商品评论用于发布在独立的商城系统上面，目前已支持大商创X版。

#### 软件架构
软件架构说明
发布数据时共有两种发布模式可选择
1.直通数据库，连接数据库后将采集到的内容直接发布到数据库
  优点:速度快，稳定性强

2.通过PHP API的形式连接数据库
  优点:

#### 使用教程

1.  采集评论
2.  采集用户
3.  批量注册

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
